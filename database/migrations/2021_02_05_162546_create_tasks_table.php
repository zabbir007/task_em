<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('protibadonName')->nullable();
            $table->integer('protibadonId')->nullable();
            $table->string('karjokromName')->nullable();
            $table->integer('karjokromId')->nullable();
            $table->string('subkarjokromName')->nullable();
            $table->integer('subkarjokromId')->nullable();
            $table->string('kormosompadonsochok')->nullable();
            $table->string('suchokman')->nullable();
            $table->string('akok')->nullable();
            $table->string('firsts')->nullable();
            $table->string('seconds')->nullable();
            $table->string('thirds')->nullable();
            $table->string('fours')->nullable();

            $table->string('firstst')->nullable();
            $table->string('secondst')->nullable();
            $table->string('thirdst')->nullable();
            $table->string('fourst')->nullable();

            $table->string('firstl')->nullable();
            $table->string('secondl')->nullable();
            $table->string('thirdl')->nullable();
            $table->string('fourl')->nullable();

            $table->string('firstp')->nullable();
            $table->string('secondp')->nullable();
            $table->string('thirdp')->nullable();
            $table->string('fourp')->nullable();
            
            $table->string('firstt')->nullable();
            $table->string('secondt')->nullable();
            $table->string('thirdt')->nullable();
            $table->string('fourt')->nullable();
            $table->string('bustobayonpod')->nullable();
            $table->string('lokkhomatra')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
