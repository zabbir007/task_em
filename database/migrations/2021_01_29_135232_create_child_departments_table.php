<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_departments', function (Blueprint $table) {
            $table->id();
            $table->integer('superParentId')->nullable();
            $table->string('superParentName')->nullable();
            $table->integer('parentId');
            $table->string('parentName')->nullable();
            $table->string('childDepartmentName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_departments');
    }
}
