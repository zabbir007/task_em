<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('taskId')->nullable();
            $table->string('protibadonName')->nullable();
            $table->integer('protibadonId')->nullable();
            $table->string('karjokromName')->nullable();
            $table->integer('karjokromId')->nullable();
            $table->string('subkarjokromName')->nullable();
            $table->integer('subkarjokromId')->nullable();
            $table->string('kormosompadonsochok')->nullable();
            $table->string('suchokman')->nullable();
            $table->string('akok')->nullable();
            $table->string('lokkhomatra')->nullable();

            $table->integer('employeeId')->nullable();
            $table->string('employeeName')->nullable();
            $table->string('bustobayonpod')->nullable();

            $table->string('first')->nullable();
            $table->string('second')->nullable();
            $table->string('third')->nullable();
            $table->string('four')->nullable();
            

            $table->string('lokkomatraorjon')->nullable();

            $table->string('lokkomatraorjonone')->nullable();
            $table->string('lokkomatraorjontwo')->nullable();
            $table->string('lokkomatraorjonthree')->nullable();
            $table->string('lokkomatraorjonfour')->nullable();

            $table->string('orjonone')->nullable();
            $table->string('orjontwo')->nullable();
            $table->string('orjonthree')->nullable();
            $table->string('orjonfour')->nullable();

            $table->string('time')->nullable();
            $table->string('timetwo')->nullable();
            $table->string('timethree')->nullable();
            $table->string('timefour')->nullable();


            $table->string('motorjon')->nullable();
            $table->string('orjitoman')->nullable();
            $table->string('montobbo')->nullable();

            $table->string('firsts')->nullable();
            $table->string('seconds')->nullable();
            $table->string('thirds')->nullable();
            $table->string('fours')->nullable();
            $table->string('firstp')->nullable();
            $table->string('secondp')->nullable();
            $table->string('thirdp')->nullable();
            $table->string('fourp')->nullable();
            $table->string('firstt')->nullable();
            $table->string('secondt')->nullable();
            $table->string('thirdt')->nullable();
            $table->string('fourt')->nullable();

            $table->string('firstst')->nullable();
            $table->string('secondst')->nullable();
            $table->string('thirdst')->nullable();
            $table->string('fourst')->nullable();

            $table->string('firstl')->nullable();
            $table->string('secondl')->nullable();
            $table->string('thirdl')->nullable();
            $table->string('fourl')->nullable();

            $table->string('statusf')->default(0);
            $table->string('statuss')->default(0);
            $table->string('statust')->default(0);
            $table->string('statusfo')->default(0);

            $table->string('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_tasks');
    }
}
