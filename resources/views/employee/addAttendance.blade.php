@extends('layouts.employee')

@section('title') Add Attendance @endsection

@section('content')

<div class="row mt-3">
    <div class="col-lg-6">
        <div class="card-box">
        	<form class="needs-validation" method="post" action="{{route('insertEmployeeAttendance')}}" novalidate>
            @csrf
	            <?php 
	                $message=Session::get('message');
	               if($message){
	            ?>
	                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                        <?php
	                            echo $message;
	                            Session::put('message','');
	                        ?>
	                    </div>
	            <?php
	                
	            	}
	            ?> 
	            @if($errors->any())
	                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                       <ul>
                           @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                           @endforeach
                       </ul>
	                </div>
                @endif
	        <div class="form-group">
                <label for="heard">Date : <?php echo date('d-m-Y'); ?></label>
            </div>
            <div class="form-group">
                <label for="heard">Select One Option *:</label>
                <select id="heard" class="form-control" required="" name="status">
                    <option value="">Choose..</option>
                    <option value="1" <?php if(!empty($attend_info)){if($attend_info->status=='1'){echo "selected";}}?> >Present</option>
                    <option value="0" <?php if(!empty($attend_info)){if($attend_info->status=='0'){echo "selected";}}?> >Absence</option>
                </select>
            </div>
            <div class="form-group row">
                    <div class="col-8 offset-4">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Submit
                        </button>
                    </div>
                </div>
	        </form>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection
