@extends('layouts.employee')

@section('title') এসাইন টাস্ক @endsection

@section('content')
    <form action="{{route('updateEmployeeTask')}}" class="parsley-examples" method="post" enctype="multipart/form-data">
    <div class="row">

        <div class="col-lg-6">
            <div class="card-box">
                <h4 class="header-title m-t-0">এসাইন টাস্ক</h4>
                @csrf
                <br/>
                <?php
                $message = Session::get('message');
                if($message){
                ?>
                <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                     role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                    echo $message;
                    Session::put('message', '');
                    ?>
                </div>
                <?php

                }
                ?>
                <?php
                $messageWarning = Session::get('messageWarning');
                if($messageWarning){
                ?>
                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                     role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                    echo $messageWarning;
                    Session::put('messageWarning', '');
                    ?>
                </div>
                <?php

                }
                ?>
                @if($errors->any())
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                         role="alert">


                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>


                    </div>
                @endif
                <div class="form-group">
                    <label for="emailAddress">প্রতিবেদনের নাম<span class="text-danger">*</span></label>
                    <input type="text" name="akok" value="{{$singleTaskInfo->protibadonName}}" disabled
                           parsley-trigger="change"
                           placeholder="" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">কার্যক্রম সমূহের নাম<span
                            class="text-danger">*</span></label>
                    <input type="text" name="" value="{{$singleTaskInfo->karjokromName}}" disabled
                           parsley-trigger="change"
                           placeholder="" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">সাব কার্যক্রম সমূহের নাম<span
                            class="text-danger">*</span></label>
                    <input type="text" name="" value="{{$singleTaskInfo->subkarjokromName}}" disabled
                           parsley-trigger="change"
                           placeholder="" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">কর্মসম্পাদন সূচক<span class="text-danger">*</span></label>
                    <input type="text" name="kormosompadonsochok" value="{{$singleTaskInfo->kormosompadonsochok}}"
                           disabled parsley-trigger="change"
                           placeholder="" class="form-control" id="userName">
                </div>
                
                <div class="form-group">
                    <label for="userName">সূচকের মান<span class="text-danger">*</span></label>
                    <input type="text" name="" value="{{$singleTaskInfo->suchokman}}" disabled parsley-trigger="change"

                           placeholder="" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">অর্থবছরের লক্ষ্যমাত্রা<span
                            class="text-danger">*</span></label>
                    <input type="text" name="" value="{{$singleTaskInfo->lokkhomatra}}" disabled
                           parsley-trigger="change"
                           placeholder="" class="form-control" id="userName">
                </div>
                <div class="form-group">
                        <label for="userName">একক<span class="text-danger">*</span></label>
                        <input type="text" name="" value="<?php
                                    if ($singleTaskInfo->akok=='1') {
                                        echo "সংখ্যা";
                                    }else if ($singleTaskInfo->akok=='2') {
                                        echo "পার্সেন্ট";
                                    }else if ($singleTaskInfo->akok=='3') {
                                        echo "তারিখ";
                                    }else if ($singleTaskInfo->akok=='4') {
                                        echo "সংখ্যা তারিখ";
                                    }else if ($singleTaskInfo->akok=='5') {
                                        echo "লক্ষ্য";
                                    }
                                ?>" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <?php
                        if ($singleTaskInfo->akok=='1') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firsts" value="{{$singleTaskInfo->firsts}}" disabled parsley-trigger="change" 
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="seconds" disabled value="{{$singleTaskInfo->seconds}}" parsley-trigger="change" 
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirds" disabled value="{{$singleTaskInfo->thirds}}" parsley-trigger="change" 
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fours" disabled value="{{$singleTaskInfo->fours}}" parsley-trigger="change" 
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='2') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firstp" value="{{$singleTaskInfo->firstp}}" disabled parsley-trigger="change" 
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="secondp" disabled value="{{$singleTaskInfo->secondp}}" parsley-trigger="change" 
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirdp" disabled value="{{$singleTaskInfo->thirdp}}" parsley-trigger="change" 
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fourp" disabled value="{{$singleTaskInfo->fourp}}" parsley-trigger="change" 
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='3') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firstt" value="{{$singleTaskInfo->firstt}}" disabled parsley-trigger="change" 
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="secondt" disabled value="{{$singleTaskInfo->secondt}}" parsley-trigger="change" 
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirdt" disabled value="{{$singleTaskInfo->thirdt}}" parsley-trigger="change" 
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fourt" disabled value="{{$singleTaskInfo->fourt}}" parsley-trigger="change" 
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                    if ($singleTaskInfo->akok=='4') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="firstst" disabled value="{{$singleTaskInfo->firstst}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="date" name="secondst" disabled value="{{$singleTaskInfo->secondst}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="thirdst" disabled value="{{$singleTaskInfo->thirdst}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="date" name="fourst" disabled value="{{$singleTaskInfo->fourst}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>

                    <?php
                    if ($singleTaskInfo->akok=='5') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="firstl" disabled value="{{$singleTaskInfo->firstl}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="secondl" disabled value="{{$singleTaskInfo->secondl}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="thirdl" disabled value="{{$singleTaskInfo->thirdl}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="fourl" disabled value="{{$singleTaskInfo->fourl}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>

            </div> <!-- end card-box -->
        </div>
        <!-- end col -->
        <div class="col-lg-6">
            <div class="card-box">

                
                <div class="form-group">
                    <label for="userName">১ম কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                    <input type="date" name="time" disabled value="{{$singleTaskInfo->time}}" parsley-trigger="change" required
                           placeholder="১ম কোয়াটার সময়সীমা" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="emailAddress">১ম কোয়াটার স্টেটাস<span
                            class="text-danger">*</span></label>
                    <select class="selectpicker" name="statusf" required data-live-search="true"
                            data-style="btn-light" tabindex="-98">
                        <option value="0" <?php if ($singleTaskInfo->statusf == '0') {echo "selected";}?> >পেন্ডিং</option>
                        <option value="2" <?php if ($singleTaskInfo->statusf == '2') {echo "selected";}?> >কমপ্লিট</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">১ম কোয়াটার অর্জন<span class="text-danger">*</span></label>
                    <input type="text" name="orjonone"  value="{{$singleTaskInfo->orjonone}}" parsley-trigger="change"
                           placeholder="১ম কোয়াটার অর্জন" class="form-control" id="userName">
                </div>
                
                <div class="form-group">
                    <label for="userName">২য় কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                    <input type="date" name="timetwo" disabled value="{{$singleTaskInfo->timetwo}}" parsley-trigger="change" required
                           placeholder="২য় কোয়াটার সময়সীমা" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="emailAddress">২য় কোয়াটার স্টেটাস<span
                            class="text-danger">*</span></label>
                    <select class="selectpicker" name="statuss" required data-live-search="true"
                            data-style="btn-light" tabindex="-98">
                        <option value="0" <?php if ($singleTaskInfo->statuss == '0') {echo "selected";}?> >পেন্ডিং</option>
                        <option value="2" <?php if ($singleTaskInfo->statuss == '2') {echo "selected";}?> >কমপ্লিট</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">২য় কোয়াটার অর্জন<span class="text-danger">*</span></label>
                    <input type="text" name="orjontwo" value="{{$singleTaskInfo->orjontwo}}" parsley-trigger="change"
                           placeholder="২য় কোয়াটার অর্জন" class="form-control" id="userName">
                </div>
                
                <div class="form-group">
                    <label for="userName">৩য় কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                    <input type="date" name="timethree" disabled value="{{$singleTaskInfo->timethree}}" parsley-trigger="change" required
                           placeholder="৩য় কোয়াটার সময়সীমা" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="emailAddress">৩য় কোয়াটার স্টেটাস<span
                            class="text-danger">*</span></label>
                    <select class="selectpicker" name="statust" required data-live-search="true"
                            data-style="btn-light" tabindex="-98">
                        <option value="0" <?php if ($singleTaskInfo->statust == '0') {echo "selected";}?> >পেন্ডিং</option>
                        <option value="2" <?php if ($singleTaskInfo->statust == '2') {echo "selected";}?> >কমপ্লিট</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">৩য় কোয়াটার অর্জন<span
                            class="text-danger">*</span></label>
                    <input type="text" name="orjonthree" value="{{$singleTaskInfo->orjonthree}}" parsley-trigger="change"
                           placeholder="৩য় কোয়াটার অর্জন" class="form-control" id="userName">
                </div>
                
                <div class="form-group">
                    <label for="userName">৪থ কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                    <input type="date" name="timefour" disabled value="{{$singleTaskInfo->timefour}}" parsley-trigger="change" required
                           placeholder="৪থ কোয়াটার সময়সীমা" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="emailAddress">৪থ কোয়াটার স্টেটাস<span
                            class="text-danger">*</span></label>
                    <select class="selectpicker" name="statusfo" required data-live-search="true"
                            data-style="btn-light" tabindex="-98">
                        <option value="0" <?php if ($singleTaskInfo->statusfo == '0') {echo "selected";}?> >পেন্ডিং</option>
                        <option value="2" <?php if ($singleTaskInfo->statusfo == '2') {echo "selected";}?> >কমপ্লিট</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">৪থ কোয়াটার অর্জন<span
                            class="text-danger">*</span></label>
                    <input type="text" name="orjonfour" value="{{$singleTaskInfo->orjonfour}}" parsley-trigger="change"
                           placeholder="৪থ কোয়াটার অর্জন" class="form-control" id="userName">
                </div>
                <input type="hidden" value="{{$singleTaskInfo->id}}" name="id">
                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        আপডেট
                    </button>
                </div>
            </div>
        </div>
    </div>
    </form>

@endsection
