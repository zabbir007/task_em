========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!-- User box -->
                    <div class="user-box text-center">
                        <img src="{{asset('admin/assets/images/logo-light.png')}}" alt="" title="" class="rounded-circle avatar-md">
                        <div class="dropdown">
                            <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">Admin</a>
                            <div class="dropdown-menu user-pro-dropdown">

                                    <i class="fe-user mr-1"></i>
                                    <span>Admin</span>
                                </a>
                                <!-- item-->
                                <a href="{{route('superAdminLogout')}}" class="dropdown-item notify-item">
                                    <i class="fe-log-out mr-1"></i>
                                    <span>Logout</span>
                                </a>

                            </div>
                        </div>
                        <p class="text-muted">
                            Admin
                        </p>
                    </div>

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title"></li>

                            <li>
                                <a href="{{route('superAdminDashboard')}}">
                                    <i class="fe-airplay"></i>

                                    <span> ড্যাশবোর্ড </span>
                                </a>

                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-airplay"></i>
                                    <span> প্রতিবেদন </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">

                                    <li>
                                        <a href="{{route('showSuperParentDepartment')}}">প্রতিবেদন সমূহ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showParentDepartment')}}">কার্যক্রম সমূহ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showChildDepartment')}}">সাব-কার্যক্রম সমূহ</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-airplay"></i>
                                    <span> কর্মকর্তা </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showEmployee')}}">কর্মকর্তা সমূহ</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-airplay"></i>
                                    <span> টাস্ক </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showTask')}}">টাস্ক সমূহ</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showAssignTask')}}">এসাইন টাস্ক</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End
