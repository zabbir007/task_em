@extends('layouts.admin')

@section('title') টাস্ক @endsection

@section('content')

    <form action="{{route('insertTask')}}" class="parsley-examples" method="post" enctype="multipart/form-data">
        <div class="row">

            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0">নতুন টাস্ক সংযুক্ত করুন</h4>
                    @csrf
                    <br/>
                    <?php
                    $message = Session::get('message');
                    if($message){
                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $message;
                        Session::put('message', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    <?php
                    $messageWarning = Session::get('messageWarning');
                    if($messageWarning){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $messageWarning;
                        Session::put('messageWarning', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                             role="alert">


                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                    <div class="form-group">
                        <label for="emailAddress">প্রতিবেদনের নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <select class="selectpicker" name="protibadonId" required data-live-search="true"
                                data-style="btn-light" tabindex="-98">
                            <option value="">Select</option>
                            @foreach($protibadonInfo as $superParentDepart)
                                <option
                                    value="{{$superParentDepart->id}}">{{$superParentDepart->super_department_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="loadingImgKarjokrom" style="display: none;">
                        <img src="{{asset('loading_img/Spinner.gif')}}"/>
                    </div>
                    <div class="form-group" id="showFieldOne" style="display:none;">
                        <label for="userName">কার্যক্রম সমূহের নাম সংযুক্ত করুন<span
                                class="text-danger">*</span></label><br>
                        <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="karjokromId" id="karjokromId">

                        </select>
                    </div>
                    <div id="loadingImgSubKarjokrom" style="display: none;">
                        <img src="{{asset('loading_img/Spinner.gif')}}"/>
                    </div>
                    <div class="form-group" id="showFieldTwo" style="display:none;">
                        <label for="userName">সাব কার্যক্রম সমূহের নাম সংযুক্ত করুন<span
                                class="text-danger">*</span></label><br>
                        <select name="subkarjokromId" id="subkarjokromId"
                                data-style="btn-light" tabindex="-98">

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="userName">কর্মসম্পাদন সূচক সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <input type="text" name="kormosompadonsochok" parsley-trigger="change" required
                               placeholder="কর্মসম্পাদন সূচক সংযুক্ত করুন" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">অর্থবছরের লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                class="text-danger">*</span></label>
                        <input type="text" name="lokkhomatra" parsley-trigger="change" required
                               placeholder="অর্থবছরের লক্ষ্যমাত্রা সংযুক্ত করুন" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">সূচকের মান সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <input type="text" name="suchokman" parsley-trigger="change" required
                               placeholder="সূচকের মান সংযুক্ত করুন" class="form-control" id="userName">
                    </div>



                </div> <!-- end card-box -->
            </div>
            <!-- end col -->
            <div class="col-lg-6">
                <div class="card-box">


                    <div class="form-group">
                        <label for="emailAddress">একক সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <select class="selectpicker" name="akok" required data-live-search="true"
                                data-style="btn-light" tabindex="-98">
                            <option value="">Select</option>
                            <option value="1">সংখ্যা</option>
                            <option value="2">পার্সেন্ট</option>
                            <option value="3">তারিখ</option>
                            <option value="4">সংখ্যা তারিখ</option>
                            <option value="5">লক্ষ্য</option>
                        </select>
                    </div>
                    <div id="s" style="display: none;">
                        <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯ 0-9]" name="firsts" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="seconds" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="thirds" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="fours" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    </div>
                    <div id="p" style="display: none;">
                        <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="firstp" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="secondp" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="thirdp" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="fourp" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    </div>
                    <div id="t" style="display: none;">
                        <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="date" name="firstt" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="date" name="secondt" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="date" name="thirdt" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="date" name="fourt" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    </div>

                    <div id="st" style="display: none;">
                        <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="firstst" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="date" name="secondst" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="thirdst" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="date" name="fourst" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    </div>
                    <div id="l" style="display: none;">
                        <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="firstl" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="secondl" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="thirdl" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                    class="text-danger">*</span></label>
                            <input type="text" pattern="[০-৯0-9]" name="fourl" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            সাবমিট
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("select[name='akok']").change(function () {
                var id = $.trim($("select[name='akok']").val());
                if (id == '1') {
                    $('#p').hide();
                    $('#t').hide();
                    $('#s').show();
                    $('#st').hide();
                    $('#l').hide();
                }else if(id == '2'){
                    $('#p').show();
                    $('#t').hide();
                    $('#s').hide();
                    $('#st').hide();
                    $('#l').hide();
                }else if(id == '3'){
                    $('#p').hide();
                    $('#t').show();
                    $('#s').hide();
                    $('#st').hide();
                    $('#l').hide();
                }else if(id == '4'){
                    $('#p').hide();
                    $('#t').hide();
                    $('#s').hide();
                    $('#st').show();
                    $('#l').hide();
                }else if(id == '5'){
                    $('#p').hide();
                    $('#t').hide();
                    $('#s').hide();
                    $('#st').hide();
                    $('#l').show();
                }
             });
            $("select[name='karjokromId']").change(function () {
                var id = $.trim($("select[name='karjokromId']").val());
                var protibadonId = $("select[name='protibadonId']").val();
                if (id == '') {
                    $('#showFieldTwo').hide();
                } else {
                    var APP_URL = $('meta[name="_base_url"]').attr('content');
                    jQuery.ajax({
                        url: APP_URL + '/admin/get-subkarjokromId',
                        method: 'post',
                        data: {id: id,protibadonId:protibadonId},
                        beforeSend: function () {
                            $('#loadingImgSubKarjokrom').show();
                        },
                        success: function (result) {
                            $('#loadingImgSubKarjokrom').hide();
                            var subkarjokrom = JSON.parse(result);
                            $("#subkarjokromId").empty();
                            var options = '<option value="">----Select----</option>';
                            $('#showFieldTwo').show();
                            $.each(subkarjokrom, function (key, val) {
                                options += '<option value=' + val.id + '>' + val.childDepartmentName + '</option>';
                            });
                            $("#subkarjokromId").append(options);

                        },
                        error: function () {
                            alert('Error occurs!');
                        }
                    });

                }
            });
            $("select[name='protibadonId']").change(function () {
                var id = $.trim($("select[name='protibadonId']").val());
                if (id == '') {
                    $('#showFieldOne').hide();
                } else {
                    var APP_URL = $('meta[name="_base_url"]').attr('content');
                    jQuery.ajax({
                        url: APP_URL + '/admin/get-karjokromId',
                        method: 'post',
                        data: {id: id},
                        beforeSend: function () {
                            $('#loadingImgKarjokrom').show();
                        },
                        success: function (result) {
                            $('#loadingImgKarjokrom').hide();
                            var karjokrom = JSON.parse(result);
                            $("#karjokromId").empty();
                            var options = '<option value="">----Select----</option>';
                            $('#showFieldOne').show();
                            $.each(karjokrom, function (key, val) {
                                options += '<option value=' + val.parentId + '>' + val.parentName + '</option>';
                            });
                            $("#karjokromId").append(options);

                        },
                        error: function () {
                            alert('Error occurs!');
                        }
                    });

                }
            });


        });
    </script>

@endsection
