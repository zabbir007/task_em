@extends('layouts.admin')

@section('title') টাস্ক @endsection

@section('content')

    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card-box">
                <div class="row">
                    <div class="col-6">
                        <h4 class="header-title">সকল টাস্ক সমূহ</h4>
                    </div>
                    <div class="col-6">
                        <a href="{{route('addTask')}}" style="margin-left: 30%;margin-top: 0px;"><button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">নতুন টাস্ক যোগ করুন</button></a>
                    </div>
                </div>
                <table id="datatable-buttons" class="table table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th>প্রতিবেদন সমূহ</th>
                            <th>কার্যক্রম সমূহ</th>
                            <th>সাব-কার্যক্রম সমূহ</th>
                            <th>কর্মসম্পাদন সূচক</th>
                            <th>সূচকের মান</th>
                            <th>একক</th>
                            <th>সম্পাদনা</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($task_info as $single_task_info)
                        <tr>
                            <td>{{$single_task_info->protibadonName}}</td>
                            <td>{{$single_task_info->karjokromName}}</td>
                            <td>{{$single_task_info->subkarjokromName}}</td>
                            <td>{{$single_task_info->kormosompadonsochok}}</td>
                            <td>{{$single_task_info->suchokman}}</td>
                            <td>
                                <?php
                                    if ($single_task_info->akok=='1') {
                                        echo "সংখ্যা";
                                    }else if ($single_task_info->akok=='2') {
                                        echo "পার্সেন্ট";
                                    }else if ($single_task_info->akok=='3') {
                                        echo "তারিখ";
                                    }else if ($single_task_info->akok=='4') {
                                        echo "সংখ্যা তারিখ";
                                    }else if ($single_task_info->akok=='5') {
                                        echo "লক্ষ্য";
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewTask',[$single_task_info->id])}}" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editTask',[$single_task_info->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" id="{{$single_task_info->id}}" class="action-icon btnDelete"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end card-box -->
        </div>
        <!-- end col -->
    </div>
    <script type="text/javascript">
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btnDelete").click(function(){
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            jQuery.ajax({
                                url: APP_URL+'/admin/Delete-Task',
                                method: 'post',
                                data:{id:id},
                                success: function(result){
                                    location.reload(true);
                                },
                                error: function() {
                                    alert('Error occurs!');
                                }
                            });
                            swal("Poof! Your  record has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your  record is safe!");
                        }
                    });
            })
        });
    </script>
@endsection
