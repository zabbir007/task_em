@extends('layouts.admin')

@section('title') টাস্ক @endsection

@section('content')

        <div class="row">
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0">টাস্ক</h4>

                    <div class="form-group">
                        <label for="emailAddress">প্রতিবেদনের নাম<span class="text-danger">*</span></label>
                        <input type="text" name="akok" value="{{$singleTaskInfo->protibadonName}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">কার্যক্রম সমূহের নাম<span
                                class="text-danger">*</span></label>
                        <input type="text" name="" value="{{$singleTaskInfo->karjokromName}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">সাব কার্যক্রম সমূহের নাম<span
                                class="text-danger">*</span></label>
                        <input type="text" name="" value="{{$singleTaskInfo->subkarjokromName}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">কর্মসম্পাদন সূচক<span class="text-danger">*</span></label>
                        <input type="text" name="kormosompadonsochok" value="{{$singleTaskInfo->kormosompadonsochok}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">অর্থবছরের লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="text" name="" value="{{$singleTaskInfo->lokkhomatra}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>

                </div> <!-- end card-box -->
            </div>
            <!-- end col -->
            <div class="col-lg-6">
                <div class="card-box">
                    <div class="form-group">
                        <label for="userName">সূচকের মান<span class="text-danger">*</span></label>
                        <input type="text" name="" value="{{$singleTaskInfo->suchokman}}" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">একক<span class="text-danger">*</span></label>
                        <input type="text" name="" value="<?php
                                    if ($singleTaskInfo->akok=='1') {
                                        echo "সংখ্যা";
                                    }else if ($singleTaskInfo->akok=='2') {
                                        echo "পার্সেন্ট";
                                    }else if ($singleTaskInfo->akok=='3') {
                                        echo "তারিখ";
                                    }else if ($singleTaskInfo->akok=='4') {
                                        echo "সংখ্যা তারিখ";
                                    }else if ($singleTaskInfo->akok=='5') {
                                        echo "লক্ষ্য";
                                    }
                                ?>" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <?php
                        if ($singleTaskInfo->akok=='1') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firsts" value="{{$singleTaskInfo->firsts}}" disabled parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="seconds" disabled value="{{$singleTaskInfo->seconds}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirds" disabled value="{{$singleTaskInfo->thirds}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fours" disabled value="{{$singleTaskInfo->fours}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='2') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firstp" value="{{$singleTaskInfo->firstp}}" disabled parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="secondp" disabled value="{{$singleTaskInfo->secondp}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirdp" disabled value="{{$singleTaskInfo->thirdp}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fourp" disabled value="{{$singleTaskInfo->fourp}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='3') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firstt" value="{{$singleTaskInfo->firstt}}" disabled parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="secondt" disabled value="{{$singleTaskInfo->secondt}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirdt" disabled value="{{$singleTaskInfo->thirdt}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fourt" disabled value="{{$singleTaskInfo->fourt}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                    if ($singleTaskInfo->akok=='4') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="firstst" disabled value="{{$singleTaskInfo->firstst}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="date" name="secondst" disabled value="{{$singleTaskInfo->secondst}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="thirdst" disabled value="{{$singleTaskInfo->thirdst}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="date" name="fourst" disabled value="{{$singleTaskInfo->fourst}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>

                    <?php
                    if ($singleTaskInfo->akok=='5') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="firstl" disabled value="{{$singleTaskInfo->firstl}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="number" name="secondl" disabled value="{{$singleTaskInfo->secondl}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="thirdl" disabled value="{{$singleTaskInfo->thirdl}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="number" name="fourl" disabled value="{{$singleTaskInfo->fourl}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>
                    
                </div>
            </div>
        </div>
@endsection
