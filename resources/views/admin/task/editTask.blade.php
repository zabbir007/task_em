@extends('layouts.admin')

@section('title') টাস্ক @endsection

@section('content')

    <form action="{{route('updateTask')}}" class="parsley-examples" method="post" enctype="multipart/form-data">
        <div class="row">

            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0">টাস্ক</h4>
                    @csrf
                    <br/>
                    <?php
                    $message = Session::get('message');
                    if($message){
                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $message;
                        Session::put('message', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    <?php
                    $messageWarning = Session::get('messageWarning');
                    if($messageWarning){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $messageWarning;
                        Session::put('messageWarning', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                             role="alert">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                    <div class="form-group">
                        <label for="emailAddress">প্রতিবেদনের নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <select class="selectpicker" name="protibadonId" data-live-search="true"
                                data-style="btn-light" tabindex="-98">
                            <option value="">Select</option>
                            @foreach($protibadonInfo as $superParentDepart)
                                <option
                                    value="{{$superParentDepart->id}}">{{$superParentDepart->super_department_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="loadingImgKarjokrom" style="display: none;">
                        <img src="{{asset('loading_img/Spinner.gif')}}"/>
                    </div>
                    <div class="form-group" id="showFieldOne" style="display:none;">
                        <label for="userName">কার্যক্রম সমূহের নাম সংযুক্ত করুন<span
                                class="text-danger">*</span></label><br>
                        <select name="karjokromId" id="karjokromId">

                        </select>
                    </div>
                    <div id="loadingImgSubKarjokrom" style="display: none;">
                        <img src="{{asset('loading_img/Spinner.gif')}}"/>
                    </div>
                    <div class="form-group" id="showFieldTwo" style="display:none;">
                        <label for="userName">সাব কার্যক্রম সমূহের নাম সংযুক্ত করুন<span
                                class="text-danger">*</span></label><br>
                        <select name="subkarjokromId" id="subkarjokromId">

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="userName">কর্মসম্পাদন সূচক সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <input type="text" name="kormosompadonsochok" value="{{$singleTaskInfo->kormosompadonsochok}}"
                               parsley-trigger="change" required
                               placeholder="কর্মসম্পাদন সূচক সংযুক্ত করুন" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">অর্থবছরের লক্ষ্যমাত্রা সংযুক্ত করুন<span
                                class="text-danger">*</span></label>
                        <input type="text" name="lokkhomatra" value="{{$singleTaskInfo->lokkhomatra}}"
                               parsley-trigger="change" required
                               placeholder="অর্থবছরের লক্ষ্যমাত্রা সংযুক্ত করুন" class="form-control" id="userName">
                        <input type="hidden" value="{{$singleTaskInfo->id}}" name="id">
                    </div>
                    <div class="form-group">
                        <label for="userName">সূচকের মান সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <input type="text" name="suchokman" value="{{$singleTaskInfo->suchokman}}"
                               parsley-trigger="change" required
                               placeholder="সূচকের মান সংযুক্ত করুন" class="form-control" id="userName">
                    </div>

                </div> <!-- end card-box -->
            </div>
            <!-- end col -->
            <div class="col-lg-6">
                <div class="card-box">

                    <div class="form-group">
                        <label for="userName">একক<span class="text-danger">*</span></label>
                        <input type="text" name="" value="<?php
                                    if ($singleTaskInfo->akok=='1') {
                                        echo "সংখ্যা";
                                    }else if ($singleTaskInfo->akok=='2') {
                                        echo "পার্সেন্ট";
                                    }else if ($singleTaskInfo->akok=='3') {
                                        echo "তারিখ";
                                    }else if ($singleTaskInfo->akok=='4') {
                                        echo "সংখ্যা তারিখ";
                                    }else if ($singleTaskInfo->akok=='5') {
                                        echo "লক্ষ্য";
                                    }
                                ?>" disabled parsley-trigger="change" required
                               placeholder="" class="form-control" id="userName">
                    </div>
                    <?php
                        if ($singleTaskInfo->akok=='1') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firsts" value="{{$singleTaskInfo->firsts}}"  parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="seconds"  value="{{$singleTaskInfo->seconds}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirds"  value="{{$singleTaskInfo->thirds}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fours" value="{{$singleTaskInfo->fours}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='2') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="firstp" value="{{$singleTaskInfo->firstp}}"  parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="text" name="secondp" value="{{$singleTaskInfo->secondp}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="thirdp" value="{{$singleTaskInfo->thirdp}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="text" name="fourp" value="{{$singleTaskInfo->fourp}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>
                    <?php
                        if ($singleTaskInfo->akok=='3') {
                    ?>
                    <div class="form-group">
                            <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="date" name="firstt" value="{{$singleTaskInfo->firstt}}" parsley-trigger="change"
                                   placeholder="১ম কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                            <input type="date" name="secondt" value="{{$singleTaskInfo->secondt}}" parsley-trigger="change"
                                   placeholder="২য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="date" name="thirdt" value="{{$singleTaskInfo->thirdt}}" parsley-trigger="change"
                                   placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                        </div>
                        <div class="form-group">
                            <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                    class="text-danger">*</span></label>
                            <input type="date" name="fourt" value="{{$singleTaskInfo->fourt}}" parsley-trigger="change"
                                   placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                        </div>
                    <?php
                        }
                    ?>

                    <?php
                    if ($singleTaskInfo->akok=='4') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="text" name="firstst" value="{{$singleTaskInfo->firstst}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="date" name="secondst" value="{{$singleTaskInfo->secondst}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="text" name="thirdst" value="{{$singleTaskInfo->thirdst}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="date" name="fourst" value="{{$singleTaskInfo->fourst}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>

                    <?php
                    if ($singleTaskInfo->akok=='5') {
                    ?>
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="text" name="firstl" value="{{$singleTaskInfo->firstl}}" parsley-trigger="change"
                               placeholder="১ম কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার লক্ষ্যমাত্রা<span class="text-danger">*</span></label>
                        <input type="text" name="secondl" value="{{$singleTaskInfo->secondl}}" parsley-trigger="change"
                               placeholder="২য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="text" name="thirdl" value="{{$singleTaskInfo->thirdl}}" parsley-trigger="change"
                               placeholder="৩য় কোয়াটার" class="form-control" id="userName">
                    </div>
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার লক্ষ্যমাত্রা<span
                                class="text-danger">*</span></label>
                        <input type="text" name="fourl" value="{{$singleTaskInfo->fourl}}" parsley-trigger="change"
                               placeholder="৪থ কোয়াটার" class="form-control" id="userName">
                    </div>
                    <?php
                    }
                    ?>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            আপডেট
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("select[name='akok']").change(function () {
                var id = $.trim($("select[name='akok']").val());
                if (id == '1') {
                    $('#p').hide();
                    $('#t').hide();
                    $('#s').show();
                }else if(id == '2'){
                    $('#p').show();
                    $('#t').hide();
                    $('#s').hide();
                }else if(id == '3'){
                    $('#p').hide();
                    $('#t').show();
                    $('#s').hide();
                }
             });
            $("select[name='karjokromId']").change(function () {
                var id = $.trim($("select[name='karjokromId']").val());
                var protibadonId = $("select[name='protibadonId']").val();
                if (id == '') {
                    $('#showFieldTwo').hide();
                } else {
                    var APP_URL = $('meta[name="_base_url"]').attr('content');
                    jQuery.ajax({
                        url: APP_URL + '/admin/get-subkarjokromId',
                        method: 'post',
                        data: {id: id, protibadonId: protibadonId},
                        beforeSend: function () {
                            $('#loadingImgSubKarjokrom').show();
                        },
                        success: function (result) {
                            $('#loadingImgSubKarjokrom').hide();
                            var subkarjokrom = JSON.parse(result);
                            $("#subkarjokromId").empty();
                            var options = '<option value="">----Select----</option>';
                            $('#showFieldTwo').show();
                            $.each(subkarjokrom, function (key, val) {
                                options += '<option value=' + val.id + '>' + val.childDepartmentName + '</option>';
                            });
                            $("#subkarjokromId").append(options);

                        },
                        error: function () {
                            alert('Error occurs!');
                        }
                    });

                }
            });
            $("select[name='protibadonId']").change(function () {
                var id = $.trim($("select[name='protibadonId']").val());
                if (id == '') {
                    $('#showFieldOne').hide();
                } else {
                    var APP_URL = $('meta[name="_base_url"]').attr('content');
                    jQuery.ajax({
                        url: APP_URL + '/admin/get-karjokromId',
                        method: 'post',
                        data: {id: id},
                        beforeSend: function () {
                            $('#loadingImgKarjokrom').show();
                        },
                        success: function (result) {
                            $('#loadingImgKarjokrom').hide();
                            var karjokrom = JSON.parse(result);
                            $("#karjokromId").empty();
                            var options = '<option value="">----Select----</option>';
                            $('#showFieldOne').show();
                            $.each(karjokrom, function (key, val) {
                                options += '<option value=' + val.parentId + '>' + val.parentName + '</option>';
                            });
                            $("#karjokromId").append(options);

                        },
                        error: function () {
                            alert('Error occurs!');
                        }
                    });

                }
            });


        });
    </script>

@endsection
