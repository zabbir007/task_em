@extends('layouts.admin')

@section('title') কর্মকর্তা @endsection

@section('content')

    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card-box">
                <div class="row">
                    <div class="col-6">
                        <h4 class="header-title">সকল কর্মকর্তা সমূহ</h4>
                    </div>
                    <div class="col-6">
                        <a href="{{route('addEmployee')}}" style="margin-left: 30%;margin-top: 0px;"><button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">নতুন কর্মকর্তা যোগ করুন</button></a>
                    </div>
                </div>
                <table id="datatable-buttons" class="table table-striped dt-responsive">
                    <thead>
                    <tr>
                        <th>নাম</th>
                        <th>ইমেইল অ্যাড্রেস</th>
                        <th>মোবাইল নম্বর</th>
                        <th>পদবী</th>
                        <th>ছবি</th>
                        <th>সম্পাদনা</th>
                    </tr>
                    </thead>


                    <tbody>
                    @foreach($employee_info as $single_employee_info)
                        <tr>
                            <td>{{$single_employee_info->employee_salary}}</td>
                            <td>{{$single_employee_info->employee_email}}</td>
                            <td>{{$single_employee_info->employee_phone}}</td>
                            <td>{{$single_employee_info->employee_designation}}</td>
                            <td>
                                <img src="{{asset( $single_employee_info->employee_image )}}" alt="{{$single_employee_info->employee_image}}"  class="rounded-circle avatar-sm">
                            </td>
                            <td>
                                <a href="{{route('editEmployee',[$single_employee_info->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" id="{{$single_employee_info->id}}" class="action-icon btnDelete"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end card-box -->
        </div>
        <!-- end col -->
    </div>
    <script type="text/javascript">
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btnDelete").click(function(){
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            jQuery.ajax({
                                url: APP_URL+'/admin/Delete-Employee',
                                method: 'post',
                                data:{id:id},
                                success: function(result){
                                    location.reload(true);
                                },
                                error: function() {
                                    alert('Error occurs!');
                                }
                            });
                            swal("Poof! Your  record has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your  record is safe!");
                        }
                    });
            })
        });
    </script>
@endsection
