@extends('layouts.admin')

@section('title') কর্মকর্তা @endsection

@section('content')

<form action="{{route('insertEmployeeData')}}" class="parsley-examples" method="post" enctype="multipart/form-data">
<div class="row">

    <div class="col-lg-6">
        <div class="card-box">
            <h4 class="header-title m-t-0">নতুন কর্মকর্তা সংযুক্ত করুন</h4>
                @csrf
                <br/>
                <?php
                    $message=Session::get('message');
                   if($message){
                ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                <?php
                    $messageWarning=Session::get('messageWarning');
                   if($messageWarning){
                ?>
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                @if($errors->any())
	                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


	                           <ul>
	                               @foreach($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                               @endforeach
	                           </ul>


	                </div>
                @endif
                <div class="form-group">
                    <label for="emailAddress">ইমেইল অ্যাড্রেস<span class="text-danger">*</span></label>
                    <input type="email" name="employee_email" parsley-trigger="change" required
                           placeholder="ইমেইল অ্যাড্রেস" class="form-control" id="emailAddress">
                </div>
                <div class="form-group">
                    <label for="userName">মোবাইল নম্বর<span class="text-danger">*</span></label>
                    <input type="text" name="employee_phone" parsley-trigger="change" required
                           placeholder="মোবাইল নম্বর" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">নাম<span class="text-danger">*</span></label>
                    <input type="text" name="employee_salary" parsley-trigger="change" required
                           placeholder="নাম" class="form-control" id="userName">
                </div>
                <div class="form-group">
                    <label for="userName">পদবী<span class="text-danger">*</span></label>
                    <input type="text" name="employee_designation" parsley-trigger="change" required
                           placeholder="পদবী" class="form-control" id="userName">
                </div>


        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
    <div class="col-lg-6">
        <div class="card-box">
            <div class="form-group">
                <label for="userName">ছবি</label>
                <input type="file" name="employee_image" class="dropify">
            </div>
            <div class="form-group">
                <label for="pass1">পাসওয়ার্ড<span class="text-danger">*</span></label>
                <input id="pass1" type="password" placeholder="পাসওয়ার্ড" required
                       class="form-control" name="password">
            </div>
            <div class="form-group">
                <label for="passWord2">কনফার্ম পাসওয়ার্ড<span class="text-danger">*</span></label>
                <input data-parsley-equalto="#pass1" type="password" required
                       placeholder="কনফার্ম পাসওয়ার্ড" class="form-control" id="passWord2" name="password_confirmation">
            </div>
            <div class="form-group text-right m-b-0">
                <button class="btn btn-primary waves-effect waves-light" type="submit">
                    সাবমিট
                </button>
            </div>
        </div>
    </div>
</div>
</form>

@endsection
