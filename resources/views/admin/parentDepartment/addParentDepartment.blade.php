@extends('layouts.admin')

@section('title') Add Department @endsection

@section('content')
<div class="row">
    <div class="col-lg-6">

        <div class="card-box">
            <h4 class="header-title m-t-0">নতুন কার্যক্রম  ব্যাবস্থা</h4>
            <form action="{{route('insertParentDepartment')}}" class="parsley-examples" method="post">
                @csrf
                <br/>
                <?php
                    $message=Session::get('message');
                   if($message){
                ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                <?php
                    $messageWarning=Session::get('messageWarning');
                   if($messageWarning){
                ?>
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                @if($errors->any())
	                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


	                           <ul>
	                               @foreach($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                               @endforeach
	                           </ul>


	                </div>
                @endif
                <div class="form-group">
                    <label for="userName">কার্যক্রমের নাম<span class="text-danger">*</span></label>
                    <input type="text" name="department_name" parsley-trigger="change" required
                           placeholder="কার্যক্রমের নাম" class="form-control" id="userName">
                </div>
                <div class="form-group text-right m-b-0">
                    <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">
	                    <span class="btn-label"><i class="mdi mdi-check-all"></i></span>সাবমিট
	                </button>
                </div>

            </form>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection
