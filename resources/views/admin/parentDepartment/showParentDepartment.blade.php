@extends('layouts.admin')

@section('title') সকল কার্যক্রম সমূহ @endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                  <div class="col-6">
                    <h4 class="header-title">সকল কার্যক্রম সমূহ</h4>
                  </div>
                  <div class="col-6">
                    <a href="{{route('addParentDepartment')}}" style="margin-left: 30%;margin-top: 0px;"><button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">কার্যক্রম সমূহ সংযুক্ত করুন</button></a>
                  </div>
                </div>
                <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>নাম</th>
                            <th>সম্পাদনা</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($departmentInfo as $department)
                        <tr>
                            <td>{{$department->department_name}}</td>
                            <td>
                            <a href="{{route('editParentDepartment',[$department->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                            <a href="#" id="{{$department->id}}" class="action-icon btnDelete"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script type="text/javascript">
	$(function(){
 	$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
 	$(".btnDelete").click(function(){
        var element=$(this);
        var id = element.attr("id");
        var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this record!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
                jQuery.ajax({
                    url: APP_URL+'/admin/Delete-Parent-Department',
                    method: 'post',
                    data:{id:id},
                    success: function(result){
                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! Your  record has been deleted!", {
              icon: "success",
            });
          } else {
            swal("Your  record is safe!");
          }
        });
    })
});
</script>
@endsection
