@extends('layouts.admin')

@section('title') সাব কার্যক্রম সমূহ @endsection

@section('content')
<div class="row">
    <div class="col-lg-6">

        <div class="card-box">
            <h4 class="header-title m-t-0">সাব কার্যক্রম সমূহ</h4>
            <form action="{{route('updateChildDepartment')}}" class="parsley-examples" method="post">
                @csrf
                <br/>
                <?php
                    $message=Session::get('message');
                   if($message){
                ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                <?php
                    $messageWarning=Session::get('messageWarning');
                   if($messageWarning){
                ?>
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                <?php

                	}
                ?>
                @if($errors->any())
	                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


	                           <ul>
	                               @foreach($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                               @endforeach
	                           </ul>


	                </div>
                @endif
                <div class="form-group">
                    <label for="userName">প্রতিবেদনের নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                    <select class="selectpicker" name="superParentId" data-live-search="true" data-style="btn-light" tabindex="-98">
                        <option value="">Select</option>
                        @foreach($superParentDepartmentInfo as $superParentDepart)
                            <option value="{{$superParentDepart->id}}" <?php if($superParentDepart->id==$singleInfo->superParentId){echo "selected";} ?> >{{$superParentDepart->super_department_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">কার্যক্রম সমূহের নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                    <select class="selectpicker" name="parentId" data-live-search="true" data-style="btn-light" tabindex="-98">
                        <option value="">Select</option>
                        @foreach($parentDepartmentInfo as $parentDepart)
                            <option value="{{$parentDepart->id}}" <?php if($parentDepart->id==$singleInfo->parentId){echo "selected";} ?> >{{$parentDepart->department_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="userName">সাব কার্যক্রম সমূহের নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                    <input type="text" name="childDepartmentName" value="{{$singleInfo->childDepartmentName}}" parsley-trigger="change" required
                           placeholder="Enter Child Department Name" class="form-control" id="userName">
                           <input type="hidden" name="id" value="{{$singleInfo->id}}">
                </div>
                <div class="form-group text-right m-b-0">
                    <button type="submit" class="btn btn-danger btn-rounded waves-effect waves-light">
	                    <span class="btn-label"><i class="mdi mdi-check-all"></i></span>আপডেট
	                </button>
                </div>

            </form>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection
