@extends('layouts.admin')

@section('title') এসাইন টাস্ক @endsection

@section('content')

    <form action="{{route('updateAssignTask')}}" class="parsley-examples" method="post" enctype="multipart/form-data">
        <div class="row">

            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0">এসাইন টাস্ক</h4>
                    @csrf
                    <br/>
                    <?php
                    $message = Session::get('message');
                    if($message){
                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $message;
                        Session::put('message', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    <?php
                    $messageWarning = Session::get('messageWarning');
                    if($messageWarning){
                    ?>
                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                         role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $messageWarning;
                        Session::put('messageWarning', '');
                        ?>
                    </div>
                    <?php

                    }
                    ?>
                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                             role="alert">


                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                    <div class="form-group">
                        <label for="emailAddress">বাস্তবায়নের দায়িত্বপ্রাপ্ত ব্যাক্তির নাম সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <select class="selectpicker" name="employeeId" required data-live-search="true"
                                data-style="btn-light" tabindex="-98">
                            <option value="">Select</option>
                            @foreach($employee_info as $employee)
                                <option value="{{$employee->id}}" <?php if($employee->id==$assign_task_info->employeeId){echo "selected";} ?> >{{$employee->employee_salary}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="emailAddress">টাস্ক সংযুক্ত করুন<span class="text-danger">*</span></label>
                        <select class="selectpicker" name="taskId" required data-live-search="true"
                                data-style="btn-light" tabindex="-98">
                            <option value="">Select</option>
                            @foreach($task_info as $task)
                                <option value="{{$task->id}}" <?php if($task->id==$assign_task_info->taskId){echo "selected";} ?>>{{$task->protibadonName.' , '.$task->karjokromName.' , '.$task->subkarjokromName}}</option>
                            @endforeach
                        </select>
                    </div>

                    
                    <div class="form-group">
                        <label for="userName">১ম কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                        <input type="date" name="time" value="{{$assign_task_info->time}}"  required
                               placeholder="১ম কোয়াটার সময়সীমা" class="form-control" id="userName">
                    </div>
                    
                    <div class="form-group">
                        <label for="userName">২য় কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                        <input type="date" name="timetwo" value="{{$assign_task_info->timetwo}}" parsley-trigger="change" required
                               placeholder="২য় কোয়াটার সময়সীমা" class="form-control" id="userName">
                               <input type="hidden" name="id" value="{{$assign_task_info->id}}">
                    </div>
                    
                    <div class="form-group">
                        <label for="userName">৩য় কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                        <input type="date" name="timethree" value="{{$assign_task_info->timethree}}" parsley-trigger="change" required
                               placeholder="৩য় কোয়াটার সময়সীমা" class="form-control" id="userName">
                    </div>
                    
                    <div class="form-group">
                        <label for="userName">৪থ কোয়াটার সময়সীমা<span class="text-danger">*</span></label>
                        <input type="date" name="timefour" value="{{$assign_task_info->timefour}}" parsley-trigger="change" required
                               placeholder="৪থ কোয়াটার সময়সীমা" class="form-control" id="userName">
                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            আপডেট
                        </button>
                    </div>


                </div> <!-- end card-box -->
            </div>
            <!-- end col -->

        </div>
    </form>

@endsection
