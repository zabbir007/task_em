@extends('layouts.admin')

@section('title') এসাইন টাস্ক @endsection

@section('content')

    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card-box">
                <div class="row">
                    <div class="col-6">
                        <h4 class="header-title">সকল এসাইন টাস্ক সমূহ</h4>
                    </div>
                    <div class="col-6">
                        <a href="{{route('addAssignTask')}}" style="margin-left: 30%;margin-top: 0px;"><button type="button" class="btn btn-warning btn-rounded waves-effect waves-light">নতুন টাস্ক এসাইন করুন</button></a>
                    </div>
                </div>
                <table id="datatable-buttons" class="table table-striped dt-responsive">
                    <thead>
                        <tr>
                            <th>প্রতিবেদনের নাম</th>
                            <th>কার্যক্রমের নাম</th>
                            <th>সাব-কার্যক্রমের নাম</th>
                            <th>বাস্তবায়নের দায়িত্বপ্রাপ্ত ব্যাক্তির নাম</th>
                            <th>১ম কোয়াটার স্টেটাস</th>
                            <th>২য় কোয়াটার স্টেটাস</th>
                            <th>৩য় কোয়াটার স্টেটাস</th>
                            <th>৪থ কোয়াটার স্টেটাস</th>
                            <th>সম্পাদনা</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($task_info as $single_task_info)
                        <?php
                            $oneOne=date('Y-m-d');
                            $twoOne=$single_task_info->time;
                            $datetime1One = new DateTime($oneOne);
                            $datetime2One = new DateTime($twoOne);
                            if ($datetime1One>$datetime2One) {
                                $daysOne = -1;
                            }else{
                                $intervalOne = $datetime1One->diff($datetime2One);
                                $daysOne = $intervalOne->format('%a');
                            }
                            

                            $oneTwo=date('Y-m-d');
                            $twoTwo=$single_task_info->timetwo;
                            $datetime1Two = new DateTime($oneTwo);
                            $datetime2Two = new DateTime($twoTwo);
                            if ($datetime1Two>$datetime2Two) {
                                $daysTwo = -1;
                            }else{
                                $intervalTwo = $datetime1Two->diff($datetime2Two);
                                $daysTwo = $intervalTwo->format('%a');
                            }

                            $oneThree=date('Y-m-d');
                            $twoThree=$single_task_info->timethree;
                            $datetime1Three = new DateTime($oneThree);
                            $datetime2Three = new DateTime($twoThree);
                            if ($datetime1Three>$datetime2Three) {
                                $daysThree = -1;
                            }else{
                                $intervalThree = $datetime1Three->diff($datetime2Three);
                                $daysThree = $intervalThree->format('%a');
                            }
                            
                            $oneFour=date('Y-m-d');
                            $twoFour=$single_task_info->timefour;
                            $datetime1Four = new DateTime($oneFour);
                            $datetime2Four = new DateTime($twoFour);
                            if ($datetime1Four>$datetime2Four) {
                                $daysFour = -1;
                            }else{
                                $intervalFour = $datetime1Four->diff($datetime2Four);
                                $daysFour = $intervalFour->format('%a');
                            }
                            
                        ?>
                        <tr>
                            <td>{{$single_task_info->protibadonName}}</td>
                            <td>{{$single_task_info->karjokromName}}</td>
                            <td>{{$single_task_info->subkarjokromName}}</td>
                            <td>{{$single_task_info->employeeName}}</td>
                            <td style="border: 5px solid white;background-color:<?php if($daysOne>5){echo "#F0F9EF !important;";}else if($daysOne==5 || $daysOne==4 || $daysOne==3 || $daysOne==2 || $daysOne==1 || $daysOne==0){echo "#FEF8F0 !important;";}else if($daysOne<0){echo "#FFF5F5 !important;";} ?>">
                                <?php if ($single_task_info->statusf == '0'){
                                ?>
                                    <span style="color: #F78B8B;font-weight: bold;">পেন্ডিং</span>
                                <?php
                                    }else{
                                ?>
                                <span style="color: #7DBB79;font-weight: bold;">কমপ্লিট</span>
                                <?php
                                    }
                                ?>
                                

                            </td>
                            <td style="border: 5px solid white;background-color:<?php if($daysTwo>5){echo "#F0F9EF !important;";}else if($daysTwo==5 || $daysTwo==4 || $daysTwo==3 || $daysTwo==2 || $daysTwo==1 || $daysTwo==0){echo "#FEF8F0 !important;";}else if($daysTwo<0){echo "#FFF5F5 !important;";} ?>">
                                <?php if ($single_task_info->statuss == '0'){
                                ?>
                                    <span style="color: #F78B8B;font-weight: bold;">পেন্ডিং</span>
                                <?php
                                    }else{
                                ?>
                                <span style="color: #7DBB79;font-weight: bold;">কমপ্লিট</span>
                                <?php
                                    }
                                ?>
                            </td>
                            <td style="border: 5px solid white; background-color:<?php if($daysThree>5){echo "#F0F9EF !important;";}else if($daysThree==5 || $daysThree==4 || $daysThree==3 || $daysThree==2 || $daysThree==1 || $daysThree==0){echo "#FEF8F0 !important;";}else if($daysThree<0){echo "#FFF5F5 !important;";} ?>">
                                <?php if ($single_task_info->statust == '0'){
                                ?>
                                    <span style="color: #F78B8B;font-weight: bold;">পেন্ডিং</span>
                                <?php
                                    }else{
                                ?>
                                <span style="color: #7DBB79;font-weight: bold;">কমপ্লিট</span>
                                <?php
                                    }
                                ?>
                            </td>
                            <td style="border: 5px solid white;background-color:<?php if($daysFour>5){echo "#F0F9EF !important;";}else if($daysFour==5 || $daysFour==4 || $daysFour==3 || $daysFour==2 || $daysFour==1 || $daysFour==0){echo "#FEF8F0 !important;";}else if($daysFour<0){echo "#FFF5F5 !important;";} ?>">
                                <?php if ($single_task_info->statusfo == '0'){
                                ?>
                                    <span style="color: #F78B8B;font-weight: bold;">পেন্ডিং</span>
                                <?php
                                    }else{
                                ?>
                                <span style="color: #7DBB79;font-weight: bold;">কমপ্লিট</span>
                                <?php
                                    }
                                ?>
                            </td>
                            
                            <td>
                                <a href="{{route('viewAssignTask',[$single_task_info->id])}}" class="action-icon"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editAssignTask',[$single_task_info->id])}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="#" id="{{$single_task_info->id}}" class="action-icon btnDelete"> <i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end card-box -->
        </div>
        <!-- end col -->
    </div>
    <script type="text/javascript">
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".btnDelete").click(function(){
                var element=$(this);
                var id = element.attr("id");
                var APP_URL = $('meta[name="_base_url"]').attr('content');
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this record!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            jQuery.ajax({
                                url: APP_URL+'/admin/Delete-assign-Task',
                                method: 'post',
                                data:{id:id},
                                success: function(result){
                                    location.reload(true);
                                },
                                error: function() {
                                    alert('Error occurs!');
                                }
                            });
                            swal("Poof! Your  record has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your  record is safe!");
                        }
                    });
            })
        });
    </script>
@endsection
