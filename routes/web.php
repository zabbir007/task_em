<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AdminController@login_page');
//admin parents route
Route::prefix('admin')->group(function () {
    Route::get('login', 'AdminController@login_page')->name('login_page');
    Route::get('Dashboard', 'SuperAdminController@superAdminDashboard')->name('superAdminDashboard');
    Route::post('check_login', 'AdminController@check_login')->name('check_login');
    Route::get('superAdminLogout', 'SuperAdminController@superAdminLogout')->name('superAdminLogout');
//admin employee route
    Route::get('Employee', 'SuperAdminController@addEmployee')->name('addEmployee');
    Route::get('All-Employee', 'SuperAdminController@showEmployee')->name('showEmployee');
    Route::get('Edit-Employee/{id}', 'SuperAdminController@editEmployee')->name('editEmployee');
    Route::post('Delete-Employee', 'SuperAdminController@deleteEmployee')->name('deleteEmployee');
    Route::post('Insert-Employee-Data', 'SuperAdminController@insertEmployeeData')->name('insertEmployeeData');
    Route::post('Update-Employee-Data', 'SuperAdminController@updateEmployeeData')->name('updateEmployeeData');
//super parent
    Route::get('Add-Super-Parent-Department', 'DepartmentController@addSuperParentDepartment')->name('addSuperParentDepartment');
    Route::get('Edit-Super-Parent-Department/{id}', 'DepartmentController@editSuperParentDepartment')->name('editSuperParentDepartment');
    Route::post('Insert-Super-Parent-Department', 'DepartmentController@insertSuperParentDepartment')->name('insertSuperParentDepartment');
    Route::post('Update-Super-Parent-Department', 'DepartmentController@updateSuperParentDepartment')->name('updateSuperParentDepartment');
    Route::get('Super-Parent-Department', 'DepartmentController@showSuperParentDepartment')->name('showSuperParentDepartment');
    Route::post('Delete-Super-Parent-Department', 'DepartmentController@deleteSuperParentDepartment')->name('deleteSuperParentDepartment');
//department
    Route::get('Add-Parent-Department', 'DepartmentController@addParentDepartment')->name('addParentDepartment');
    Route::get('Edit-Parent-Department/{id}', 'DepartmentController@editParentDepartment')->name('editParentDepartment');
    Route::post('Insert-Parent-Department', 'DepartmentController@insertParentDepartment')->name('insertParentDepartment');
    Route::post('Update-Parent-Department', 'DepartmentController@updateParentDepartment')->name('updateParentDepartment');
    Route::get('Parent-Department', 'DepartmentController@showParentDepartment')->name('showParentDepartment');
    Route::post('Delete-Parent-Department', 'DepartmentController@deleteParentDepartment')->name('deleteParentDepartment');
//child department
    Route::get('Child-Department', 'DepartmentController@showChildDepartment')->name('showChildDepartment');
    Route::get('Add-Child-Department', 'DepartmentController@addChildDepartment')->name('addChildDepartment');
    Route::get('Edit-Child-Department/{id}', 'DepartmentController@editChildDepartment')->name('editChildDepartment');
    Route::post('Insert-Child-Department', 'DepartmentController@insertChildDepartment')->name('insertChildDepartment');
    Route::post('Update-Child-Department', 'DepartmentController@updateChildDepartment')->name('updateChildDepartment');
    Route::post('Delete-Child-Department', 'DepartmentController@deleteChildDepartment')->name('deleteChildDepartment');
//Task panel
    Route::get('All-task', 'AdminTaskController@showTask')->name('showTask');
    Route::get('Add-task', 'AdminTaskController@addTask')->name('addTask');
    Route::post('Insert-task', 'AdminTaskController@insertTask')->name('insertTask');
    Route::post('get-karjokromId', 'AdminTaskController@getKarjokromId')->name('getKarjokromId');
    Route::post('get-subkarjokromId', 'AdminTaskController@getsubkarjokromId')->name('getsubkarjokromId');
    Route::post('Delete-Task', 'AdminTaskController@deleteTask')->name('deleteTask');
    Route::get('View-task/{id}', 'AdminTaskController@viewTask')->name('viewTask');
    Route::get('edit-task/{id}', 'AdminTaskController@editTask')->name('editTask');
    Route::post('Update-task', 'AdminTaskController@updateTask')->name('updateTask');
//Assign task
    Route::get('All-assign-task', 'AdminTaskController@showAssignTask')->name('showAssignTask');
    Route::get('Add-assign-task', 'AdminTaskController@addAssignTask')->name('addAssignTask');
    Route::post('Insert-assign-task', 'AdminTaskController@insertAssignTask')->name('insertAssignTask');
    Route::post('Delete-assign-Task', 'AdminTaskController@deleteAssignTask')->name('deleteAssignTask');
    Route::get('View-assign-task/{id}', 'AdminTaskController@viewAssignTask')->name('viewAssignTask');
    Route::get('edit-assign-task/{id}', 'AdminTaskController@editAssignTask')->name('editAssignTask');
    Route::post('Update-assign-task', 'AdminTaskController@updateAssignTask')->name('updateAssignTask');
});

//employee parent route
Route::prefix('employee')->group(function () {
    Route::get('login', 'EmployeeController@employee_login_page')->name('employee_login_page');
    Route::get('Employee-Dashboard', 'SuperEmployeeController@superEmployeeDashboard')->name('superEmployeeDashboard');
    Route::post('employee_check_login', 'EmployeeController@employee_check_login')->name('employee_check_login');
    Route::get('superEmployeeLogout', 'SuperEmployeeController@superEmployeeLogout')->name('superEmployeeLogout');
    Route::post('update-employee-information', 'SuperEmployeeController@updateEmployeeInformation')->name('updateEmployeeInformation');
    //task
    Route::get('All-assign-task', 'SuperEmployeeController@showEmployeeTask')->name('showEmployeeTask');
    Route::get('View-assign-task/{id}', 'SuperEmployeeController@viewEmployeeTask')->name('viewEmployeeTask');
    Route::get('edit-assign-task/{id}', 'SuperEmployeeController@editEmployeeTask')->name('editEmployeeTask');
    Route::post('Update-assign-task', 'SuperEmployeeController@updateEmployeeTask')->name('updateEmployeeTask');

});
