<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION))
{
    session_start();
}
date_default_timezone_set('Asia/Dhaka');
class DepartmentController extends Controller
{
    public function __construct(){

        $this->middleware('checkAdmin');
    }
    //Super parents
    public function showSuperParentDepartment(){
        $departmentInfo=DB::table('super_departments')
            ->get();
        return view('admin.superParentDepartment.showSuperParentDepartment',compact('departmentInfo'));
    }
    public function addSuperParentDepartment(){
        return view('admin.superParentDepartment.addSuperParentDepartment');
    }
    public function insertSuperParentDepartment(Request $request){
        $request->validate([
            'super_department_name' => 'required'
        ]);
        $data=array();
        $data['super_department_name']=$request->super_department_name;
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $insertDepartmentData=DB::table('super_departments')
            ->insert($data);
        if ($insertDepartmentData){
            Session::put('message','প্রতিবেদন সমূহ সফলভাবে ইনসার্ট হয়েছে !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','প্রতিবেদন সমূহ সফলভাবে ইনসার্ট হয়নি !!!!!!');
            return redirect()->back();
        }
    }
    public function editSuperParentDepartment($id){
        $singleInfo=DB::table('super_departments')
            ->first();
        return view('admin.superParentDepartment.editSuperParentDepartment',compact('singleInfo'));
    }
    public function updateSuperParentDepartment(Request $request){
        $id=$request->id;
        $request->validate([
            'super_department_name' => 'required'
        ]);
        $data=array();
        $data['super_department_name']=$request->super_department_name;
        $data['updated_at']=date('Y-m-d');
        $updateDepartmentData=DB::table('super_departments')
            ->where('id',$id)
            ->update($data);
        Session::put('message','প্রতিবেদন সমূহ সফলভাবে আপডেট হয়েছে !!');
        return redirect()->back();

    }
    public function deleteSuperParentDepartment(Request $request){
        $id=intval($request->id);
        DB::table('super_departments')
            ->where('id', $id)
            ->delete();

    }
    //Parent
    public function showParentDepartment(){
    	$departmentInfo=DB::table('departments')
    						->get();
		return view('admin.parentDepartment.showParentDepartment',compact('departmentInfo'));
    }
    public function addParentDepartment(){
    	return view('admin.parentDepartment.addParentDepartment');
    }
    public function insertParentDepartment(Request $request){
    	$request->validate([
            'department_name' => 'required'
        ]);
        $data=array();
        $data['department_name']=$request->department_name;
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $insertDepartmentData=DB::table('departments')
                                	->insert($data);
        if ($insertDepartmentData){
            Session::put('message','কার্যক্রম সমূহ সফলভাবে ইনসার্ট হয়েছে!!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','কার্যক্রম সমূহ সফলভাবে ইনসার্ট হয়নি!!!!!!');
            return redirect()->back();
        }
    }
    public function editParentDepartment($id){
    	$singleInfo=DB::table('departments')
						->first();
    	return view('admin.parentDepartment.editParentDepartment',compact('singleInfo'));
    }
    public function updateParentDepartment(Request $request){
    	$id=$request->id;
    	$request->validate([
            'department_name' => 'required'
        ]);
        $data=array();
        $data['department_name']=$request->department_name;
        $data['updated_at']=date('Y-m-d');
        $updateDepartmentData=DB::table('departments')
        						->where('id',$id)
                            	->update($data);
        Session::put('message','কার্যক্রম সমূহ সফলভাবে আপডেট হয়েছে!!');
        return redirect()->back();

    }
    public function deleteParentDepartment(Request $request){
    	$id=intval($request->id);
    	DB::table('departments')
                    ->where('id', $id)
                    ->delete();

    }

    //Child Department
    public function showChildDepartment(){
    	$departmentInfo=DB::table('child_departments')
    						->get();
		return view('admin.childDepartment.showChildDepartment',compact('departmentInfo'));
    }
    public function addChildDepartment(){
        $parentDepartmentInfo=DB::table('departments')
                                    ->get();
        $superParentDepartmentInfo=DB::table('super_departments')
                                    ->get();
    	return view('admin.childDepartment.addChildDepartment',compact('parentDepartmentInfo','superParentDepartmentInfo'));
    }
    public function insertChildDepartment(Request $request){
    	$request->validate([
            'childDepartmentName' => 'required',
            'parentId' => 'required',
            'superParentId' => 'required'
        ]);
        $parentDepartmentName=DB::table('departments')
                                    ->where('id',$request->parentId)
                                    ->select('department_name')
                                    ->first();
        $SuperParentDepartmentName=DB::table('super_departments')
                                    ->where('id',$request->superParentId)
                                    ->select('super_department_name')
                                    ->first();

        $data=array();
        $data['childDepartmentName']=$request->childDepartmentName;
        $data['parentId']=$request->parentId;
        $data['parentName']=$parentDepartmentName->department_name;
        $data['superParentId']=$request->superParentId;
        $data['superParentName']=$SuperParentDepartmentName->super_department_name;
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $insertDepartmentData=DB::table('child_departments')
                                	->insert($data);
        if ($insertDepartmentData){
            Session::put('message','সাব কার্যক্রম সমূহ সফলভাবে ইনসার্ট হয়েছে!!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','সাব কার্যক্রম সমূহ সফলভাবে ইনসার্ট হয়নি!!!!!!');
            return redirect()->back();
        }
    }
    public function editChildDepartment($id){
        $parentDepartmentInfo=DB::table('departments')
                                    ->get();
        $superParentDepartmentInfo=DB::table('super_departments')
                                        ->get();
    	$singleInfo=DB::table('child_departments')
                        ->where('id',$id)
						->first();
    	return view('admin.childDepartment.editChildDepartment',compact('singleInfo','parentDepartmentInfo','superParentDepartmentInfo'));
    }
    public function updateChildDepartment(Request $request){
        $id=$request->id;
    	$request->validate([
            'childDepartmentName' => 'required',
            'parentId' => 'required',
            'superParentId' => 'required'
        ]);
        $parentDepartmentName=DB::table('departments')
                                    ->where('id',$request->parentId)
                                    ->select('department_name')
                                    ->first();
        $SuperParentDepartmentName=DB::table('super_departments')
                                    ->where('id',$request->superParentId)
                                    ->select('super_department_name')
                                    ->first();

        $data=array();
        $data['childDepartmentName']=$request->childDepartmentName;
        $data['parentId']=$request->parentId;
        $data['parentName']=$parentDepartmentName->department_name;
        $data['superParentId']=$request->superParentId;
        $data['superParentName']=$SuperParentDepartmentName->super_department_name;
        $data['updated_at']=date('Y-m-d');
        $updateDepartmentData=DB::table('child_departments')
        						->where('id',$id)
                            	->update($data);
        Session::put('message','সাব কার্যক্রম সমূহ সফলভাবে আপডেট হয়েছে!!');
        return redirect()->back();

    }
    public function deleteChildDepartment(Request $request){
    	$id=intval($request->id);
    	DB::table('child_departments')
                    ->where('id', $id)
                    ->delete();

    }
}
