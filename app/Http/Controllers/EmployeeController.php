<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class EmployeeController extends Controller
{
    //use middleware to check employee login or not.
    public function __construct(){
        $this->middleware('checkEmployeeLogin');
    }
    //employee login page open
    public function employee_login_page(){
        return view('employee.employeeLoginPage');
    }
    //check login
    public function employee_check_login(Request $request){
    	//validation 
    	$request->validate([
		    'employee_email' => 'required',
		    'employee_password' => 'required',
		]);
		$employee_email=$request->employee_email;
    	$employee_password=md5($request->employee_password);
    	$singleEmployee = DB::table('employees')
			    				->where('employee_email', $employee_email)
			    				->where('employee_password', $employee_password)
			    				->first();
		if ($singleEmployee) {
			Session::put('employeeId',$singleEmployee->id);
			return redirect()->route('superEmployeeDashboard');
		}else{
			Session::put('message','Email or Password Invalid');
			return redirect()->route('employee_login_page');
		}
    }
}
