<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Mail;
if (!isset($_SESSION)) {
    session_start();
}
date_default_timezone_set('Asia/Dhaka');

class AdminTaskController extends Controller
{
    public function __construct()
    {

        $this->middleware('checkAdmin');
    }

    public function showTask()
    {
        $task_info = DB::table('tasks')
            ->get();
        return view('admin.task.showTask', compact('task_info'));
    }

    public function addTask()
    {
        $protibadonInfo = DB::table('super_departments')
            ->get();
        return view('admin.task.addTask', compact('protibadonInfo'));
    }

    public function insertTask(Request $request)
    {
        $request->validate([
            'protibadonId' => 'required',
            'karjokromId' => 'required',
            'subkarjokromId' => 'required',
            'kormosompadonsochok' => 'required',
            'suchokman' => 'required',
            'akok' => 'required',
            'lokkhomatra' => 'required',
        ]);
        $data = array();
        $data['firsts'] = $request->firsts;
        $data['seconds'] = $request->seconds;
        $data['thirds'] = $request->thirds;
        $data['fours'] = $request->fours;

        $data['firstst'] = $request->firstst;
        $data['secondst'] = $request->secondst;
        $data['thirdst'] = $request->thirdst;
        $data['fourst'] = $request->fourst;

        $data['firstl'] = $request->firstl;
        $data['secondl'] = $request->secondl;
        $data['thirdl'] = $request->thirdl;
        $data['fourl'] = $request->fourl;

        $data['firstp'] = $request->firstp;
        $data['secondp'] = $request->secondp;
        $data['thirdp'] = $request->thirdp;
        $data['fourp'] = $request->fourp;
        $data['firstt'] = $request->firstt;
        $data['secondt'] = $request->secondt;
        $data['thirdt'] = $request->thirdt;
        $data['fourt'] = $request->fourt;
        $protibadonName = DB::table('child_departments')
            ->where('superParentId', $request->protibadonId)
            ->first();
        $data['protibadonName'] = $protibadonName->superParentName;
        $data['protibadonId'] = $request->protibadonId;
        $karjokromName = DB::table('child_departments')
            ->where('parentId', $request->karjokromId)
            ->first();
        $data['karjokromName'] = $karjokromName->parentName;
        $data['karjokromId'] = $request->karjokromId;
        $subkarjokromName = DB::table('child_departments')
            ->where('id', $request->subkarjokromId)
            ->first();
        $data['subkarjokromName'] = $subkarjokromName->childDepartmentName;
        $data['subkarjokromId'] = $request->subkarjokromId;
        $data['kormosompadonsochok'] = $request->kormosompadonsochok;
        $data['suchokman'] = $request->suchokman;
        $data['akok'] = $request->akok;
        $data['lokkhomatra'] = $request->lokkhomatra;
        $insertTaskData = DB::table('tasks')
            ->insert($data);
        if ($insertTaskData) {
            Session::put('message', 'টাস্ক সমূহ সফলভাবে ইনসার্ট হয়েছে!!');
            return redirect()->back();
        } else {
            Session::put('messageWarning', 'টাস্ক সমূহ সফলভাবে ইনসার্ট হয়নি!!!!!!');
            return redirect()->back();
        }
    }

    public function updateTask(Request $request)
    {

        $info = DB::table('tasks')
            ->where('id', $request->id)
            ->first();
        $protibadonId = $request->protibadonId;
        $data = array();
        $data['firsts'] = $request->firsts;
        $data['seconds'] = $request->seconds;
        $data['thirds'] = $request->thirds;
        $data['fours'] = $request->fours;
        $data['firstp'] = $request->firstp;
        $data['secondp'] = $request->secondp;
        $data['thirdp'] = $request->thirdp;
        $data['fourp'] = $request->fourp;
        $data['firstt'] = $request->firstt;
        $data['secondt'] = $request->secondt;
        $data['thirdt'] = $request->thirdt;
        $data['fourt'] = $request->fourt;

        $data['firstst'] = $request->firstst;
        $data['secondst'] = $request->secondst;
        $data['thirdst'] = $request->thirdst;
        $data['fourst'] = $request->fourst;

        $data['firstl'] = $request->firstl;
        $data['secondl'] = $request->secondl;
        $data['thirdl'] = $request->thirdl;
        $data['fourl'] = $request->fourl;

        if ($protibadonId) {
            $request->validate([
                'protibadonId' => 'required',
                'karjokromId' => 'required',
                'subkarjokromId' => 'required',
                'kormosompadonsochok' => 'required',
                'suchokman' => 'required',
                'lokkhomatra' => 'required',
            ]);
            $protibadonName = DB::table('child_departments')
                ->where('superParentId', $request->protibadonId)
                ->first();
            $data['protibadonName'] = $protibadonName->superParentName;
            $data['protibadonId'] = $request->protibadonId;
            $karjokromName = DB::table('child_departments')
                ->where('parentId', $request->karjokromId)
                ->first();
            $data['karjokromName'] = $karjokromName->parentName;
            $data['karjokromId'] = $request->karjokromId;
            $subkarjokromName = DB::table('child_departments')
                ->where('id', $request->subkarjokromId)
                ->first();
            $data['subkarjokromName'] = $subkarjokromName->childDepartmentName;
            $data['subkarjokromId'] = $request->subkarjokromId;
        } else {
            $request->validate([
                'kormosompadonsochok' => 'required',
                'suchokman' => 'required',
                'lokkhomatra' => 'required',
            ]);
            $data['protibadonName'] = $info->protibadonName;
            $data['protibadonId'] = $info->protibadonId;
            $data['karjokromName'] = $info->karjokromName;
            $data['karjokromId'] = $info->karjokromId;
            $data['subkarjokromName'] = $info->subkarjokromName;
            $data['subkarjokromId'] = $info->subkarjokromId;

        }

        $data['kormosompadonsochok'] = $request->kormosompadonsochok;
        $data['suchokman'] = $request->suchokman;
        $data['lokkhomatra'] = $request->lokkhomatra;
        DB::table('tasks')
            ->where('id', $request->id)
            ->update($data);
        Session::put('message', 'টাস্ক সমূহ সফলভাবে আপডেট হয়েছে!!');
        return redirect()->back();

    }

    public function deleteTask(Request $request)
    {
        $id = intval($request->id);
        DB::table('tasks')
            ->where('id', $id)
            ->delete();
    }

    public function viewTask($id)
    {
        $singleTaskInfo = DB::table('tasks')
            ->where('id', $id)
            ->first();
        return view('admin.task.viewTask', compact('singleTaskInfo'));
    }

    public function editTask($id)
    {
        $singleTaskInfo = DB::table('tasks')
            ->where('id', $id)
            ->first();
        $protibadonInfo = DB::table('super_departments')
            ->get();
        return view('admin.task.editTask', compact('singleTaskInfo', 'protibadonInfo'));
    }

    public function getKarjokromId(Request $request){
        $id=intval($request->id);
        $KarjokromInfo=DB::table('child_departments')
        				->select('child_departments.*')
                        ->where('superParentId',$id)
                        ->groupBy('child_departments.parentId')
                        ->get();
        echo json_encode($KarjokromInfo);
    }

    public function getsubkarjokromId(Request $request)
    {
        $id = intval($request->id);
        $protibadonId = intval($request->protibadonId);
        $subkarjokromInfo = DB::table('child_departments')
            ->where('superParentId', $protibadonId)
            ->where('parentId', $id)
            ->get();
        echo json_encode($subkarjokromInfo);
    }
    public function showAssignTask(){
        $task_info = DB::table('assign_tasks')
            ->get();
        return view('admin.assign_task.showAssignTask', compact('task_info'));
    }
    public function addAssignTask()
    {
        $employee_info = DB::table('employees')
                        ->get();
        $task_info = DB::table('tasks')
                        ->get();
        return view('admin.assign_task.addAssignTask', compact('task_info','employee_info'));
    }

    public function insertAssignTask(Request $request)
    {
        $request->validate([
            'employeeId' => 'required',
            'taskId' => 'required',

        ]);
        $data = array();
         $data['employeeId'] = $request->employeeId;
         $data['taskId'] = $request->taskId;
        $data['lokkomatraorjon'] = $request->lokkomatraorjon;
        $data['lokkomatraorjonone'] = $request->lokkomatraorjonone;
        $data['lokkomatraorjontwo'] = $request->lokkomatraorjontwo;
        $data['lokkomatraorjonthree'] = $request->lokkomatraorjonthree;
        $data['lokkomatraorjonfour'] = $request->lokkomatraorjonfour;
        $data['time'] = $request->time;
        $data['timetwo'] = $request->timetwo;
        $data['timethree'] = $request->timethree;
        $data['timefour'] = $request->timefour;

        $data['first'] = $request->first;
         $data['second'] = $request->second;
         $data['third'] = $request->third;
         $data['four'] = $request->four;

        $taskInfo = DB::table('tasks')
            ->where('id', $request->taskId)
            ->first();
        $data['protibadonName'] = $taskInfo->protibadonName;
        $data['protibadonId'] = $taskInfo->protibadonId;
        $data['karjokromName'] = $taskInfo->karjokromName;
        $data['karjokromId'] = $taskInfo->karjokromId;
        $data['subkarjokromName'] = $taskInfo->subkarjokromName;
        $data['subkarjokromId'] = $taskInfo->subkarjokromId;
        $data['kormosompadonsochok'] = $taskInfo->kormosompadonsochok;
        $data['suchokman'] = $taskInfo->suchokman;
        $data['akok'] = $taskInfo->akok;
        $data['lokkhomatra'] = $taskInfo->lokkhomatra;
        $data['firsts'] = $taskInfo->firsts;
        $data['seconds'] = $taskInfo->seconds;
        $data['thirds'] = $taskInfo->thirds;
        $data['fours'] = $taskInfo->fours;

        $data['firstst'] = $taskInfo->firstst;
        $data['secondst'] = $taskInfo->secondst;
        $data['thirdst'] = $taskInfo->thirdst;
        $data['fourst'] = $taskInfo->fourst;

        $data['firstl'] = $taskInfo->firstl;
        $data['secondl'] = $taskInfo->secondl;
        $data['thirdl'] = $taskInfo->thirdl;
        $data['fourl'] = $taskInfo->fourl;

        $data['firstp'] = $taskInfo->firstp;
        $data['secondp'] = $taskInfo->secondp;
        $data['thirdp'] = $taskInfo->thirdp;
        $data['fourp'] = $taskInfo->fourp;
        $data['firstt'] = $taskInfo->firstt;
        $data['secondt'] = $taskInfo->secondt;
        $data['thirdt'] = $taskInfo->thirdt;
        $data['fourt'] = $taskInfo->fourt;
        $employeeInfo = DB::table('employees')
                    ->where('id', $request->employeeId)
                    ->first();
        $employee_mobile=$employeeInfo->employee_phone;
        $employee_email=$employeeInfo->employee_email;
        
        
          $messageBody = '<html><body>';
          $messageBody .="<br>";
          $messageBody .= '<h1>একটি নতুন টাস্ক এসাইন করা হয়েছে</h1>';
          $messageBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
          $messageBody .= "<tr style='background: #eee;'><td><strong>প্রতিবেদনের নাম:</strong> </td><td>" . $taskInfo->protibadonName . "</td></tr>";
          $messageBody .= "<tr><td><strong>কার্যক্রমের নাম:</strong> </td><td>" . $taskInfo->karjokromName . "</td></tr>";
          $messageBody .= "<tr><td><strong>সাব-কার্যক্রমের নাম:</strong> </td><td>" .$taskInfo->subkarjokromName . "</td></tr>";
        
          $messageBody .= "<tr><td><strong>১ম কোয়াটার সময়সীমা:</strong> </td><td>" . $request->time . "</td></tr>";
           
          $messageBody .= "<tr><td><strong>২য় কোয়াটার সময়সীমা:</strong> </td><td>" . $request->timetwo . "</td></tr>";
          
          $messageBody .= "<tr><td><strong>৩য় কোয়াটার সময়সীমা:</strong> </td><td>" . $request->timethree . "</td></tr>";
         
          $messageBody .= "<tr><td><strong>৪থ কোয়াটার সময়সীমা:</strong> </td><td>" . $request->timefour . "</td></tr>";
          $messageBody .= "</table>";
          $messageBody .= "</body></html>";
          
          $this->messageCustomer($employee_mobile);
           Mail::send([], [],function ($message) use ($employee_email,$messageBody) {

               $message->from('task@dcrowd.net', 'টাস্ক');
               $message->subject("নতুন টাস্ক এসাইন");
               $message->setBody($messageBody, 'text/html');
               $message->to($employee_email);
            });
        $data['employeeName'] = $employeeInfo->employee_salary;
        $data['bustobayonpod'] = $employeeInfo->employee_designation;
        $insertAssignTaskData = DB::table('assign_tasks')
            					->insert($data);
        if ($insertAssignTaskData) {
            Session::put('message', 'টাস্ক সফলভাবে এসাইন হয়েছে!!');
            return redirect()->back();
        } else {
            Session::put('messageWarning', 'টাস্ক সফলভাবে এসাইন হয়নি!!!!!!');
            return redirect()->back();
        }
    }
    private function messageCustomer($employee_mobile){
            $api_key = 'ZGNyb3dkOjAxODYxMTUwMjky';
            $from = '8804445620747';
            $destination = $employee_mobile;
            $url = 'http://services.smsq.global/sms/api';
            $sms = 'একটি নতুন টাস্ক এসাইন করা হয়েছে';
            $sms_body = array(
                'action' => 'send-sms',
                'api_key' => $api_key,
                'to' => $destination,
                'from' => $from,
                'sms' => $sms
            );
            $send_data = http_build_query($sms_body);
            $gateway_url = $url . "?" . $send_data;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $gateway_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPGET, 1);
            $output = curl_exec($ch);

            if (curl_errno($ch)) {
                $output = curl_error($ch);
            }
            curl_close($ch);
            
    }

    public function updateAssignTask(Request $request)
    {

        $request->validate([
            'employeeId' => 'required',
            'taskId' => 'required',
            'time' => 'required',
        ]);
        $data = array();
         $data['employeeId'] = $request->employeeId;
         $data['taskId'] = $request->taskId;
         $data['lokkomatraorjon'] = $request->lokkomatraorjon;
        $data['lokkomatraorjonone'] = $request->lokkomatraorjonone;
        $data['lokkomatraorjontwo'] = $request->lokkomatraorjontwo;
        $data['lokkomatraorjonthree'] = $request->lokkomatraorjonthree;
        $data['lokkomatraorjonfour'] = $request->lokkomatraorjonfour;
        $data['time'] = $request->time;
        $data['timetwo'] = $request->timetwo;
        $data['timethree'] = $request->timethree;
        $data['timefour'] = $request->timefour;
         $data['time'] = $request->time;
         $data['first'] = $request->first;
         $data['second'] = $request->second;
         $data['third'] = $request->third;
         $data['four'] = $request->four;
        $taskInfo = DB::table('tasks')
            ->where('id', $request->taskId)
            ->first();
        $data['protibadonName'] = $taskInfo->protibadonName;
        $data['protibadonId'] = $taskInfo->protibadonId;
        $data['karjokromName'] = $taskInfo->karjokromName;
        $data['karjokromId'] = $taskInfo->karjokromId;
        $data['subkarjokromName'] = $taskInfo->subkarjokromName;
        $data['subkarjokromId'] = $taskInfo->subkarjokromId;
        $data['kormosompadonsochok'] = $taskInfo->kormosompadonsochok;
        $data['suchokman'] = $taskInfo->suchokman;
        $data['akok'] = $taskInfo->akok;
        $data['firsts'] = $taskInfo->firsts;
        $data['seconds'] = $taskInfo->seconds;
        $data['thirds'] = $taskInfo->thirds;
        $data['fours'] = $taskInfo->fours;
        $data['firstp'] = $taskInfo->firstp;
        $data['secondp'] = $taskInfo->secondp;
        $data['thirdp'] = $taskInfo->thirdp;
        $data['fourp'] = $taskInfo->fourp;
        $data['firstt'] = $taskInfo->firstt;
        $data['secondt'] = $taskInfo->secondt;
        $data['thirdt'] = $taskInfo->thirdt;
        $data['fourt'] = $taskInfo->fourt;
        $data['firstst'] = $taskInfo->firstst;
        $data['secondst'] = $taskInfo->secondst;
        $data['thirdst'] = $taskInfo->thirdst;
        $data['fourst'] = $taskInfo->fourst;

        $data['firstl'] = $taskInfo->firstl;
        $data['secondl'] = $taskInfo->secondl;
        $data['thirdl'] = $taskInfo->thirdl;
        $data['fourl'] = $taskInfo->fourl;
        $data['lokkhomatra'] = $taskInfo->lokkhomatra;
        $employeeInfo = DB::table('employees')
                    ->where('id', $request->employeeId)
                    ->first();
        $data['employeeName'] = $employeeInfo->employee_salary;
        $data['bustobayonpod'] = $employeeInfo->employee_designation;
        DB::table('assign_tasks')
            ->where('id', $request->id)
            ->update($data);
        Session::put('message', 'এসাইন টাস্ক সফলভাবে আপডেট হয়েছে!!');
        return redirect()->back();

    }

    public function deleteAssignTask(Request $request)
    {
        $id = intval($request->id);
        DB::table('assign_tasks')
            ->where('id', $id)
            ->delete();
    }

    public function viewAssignTask($id)
    {
        $singleTaskInfo = DB::table('assign_tasks')
            ->where('id', $id)
            ->first();
        return view('admin.assign_task.viewAssignTask', compact('singleTaskInfo'));
    }

    public function editAssignTask($id)
    {
        $employee_info = DB::table('employees')
                        ->get();
        $task_info = DB::table('tasks')
                        ->get();
        $assign_task_info = DB::table('assign_tasks')
        					->where('id',$id)
            				->first();
        return view('admin.assign_task.editAssignTask', compact('assign_task_info', 'employee_info','task_info'));
    }

}
