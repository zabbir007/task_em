<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION))
{
    session_start();
}
date_default_timezone_set('Asia/Dhaka');
class SuperEmployeeController extends Controller
{
    public function __construct(){

        $this->middleware('checkEmployee');
    }
    //show dashboard
    public function superEmployeeDashboard(){
        $employee_id=Session::get('employeeId');
        $department_info=DB::table('departments')
                            ->get();
        $assignTaskCount=DB::table('assign_tasks')
                            ->where('employeeId',$employee_id)
                            ->count();
        $pendingTaskCount=DB::table('assign_tasks')
                            ->where('employeeId',$employee_id)
                            ->where('statusf',0)
                            ->where('statuss',0)
                            ->where('statust',0)
                            ->where('statusfo',0)
                            ->count();
        $completeTaskCount=DB::table('assign_tasks')
                            ->where('employeeId',$employee_id)
                            ->where('statusf',1)
                            ->where('statuss',1)
                            ->where('statust',1)
                            ->where('statusfo',1)
                            ->count();
        $employee_info=DB::table('employees')
                            ->where('id',$employee_id)
                            ->first();

        // echo "<pre/>";
        // print_r($attendance);
        // exit();
    	return view('employee.employeeDashboard',compact('department_info','employee_info','assignTaskCount','pendingTaskCount','completeTaskCount'));
    }
    //employee logout
    public function superEmployeeLogout(){
    	Session::put('employeeId','');
        return redirect()->route('employee_login_page');
    }

    //task
    public function showEmployeeTask(){
        $employee_id=Session::get('employeeId');
        $task_info = DB::table('assign_tasks')
                            ->where('employeeId',$employee_id)
                            ->get();
        return view('employee.assign_task.showAssignTask', compact('task_info'));
    }
    public function viewEmployeeTask($id)
    {
        $employee_id=Session::get('employeeId');
        $singleTaskInfo = DB::table('assign_tasks')
            ->where('employeeId',$employee_id)
            ->where('id', $id)
            ->first();
        return view('employee.assign_task.viewAssignTask', compact('singleTaskInfo'));
    }
    public function editEmployeeTask($id)
    {

        $employee_id=Session::get('employeeId');
        $singleTaskInfo = DB::table('assign_tasks')
            ->where('employeeId',$employee_id)
            ->where('id',$id)
            ->first();
        return view('employee.assign_task.editAssignTask', compact('singleTaskInfo'));
    }
    public function updateEmployeeTask(Request $request){
        $data=array();
        $data['statusf']=$request->statusf;
        $data['statuss']=$request->statuss;
        $data['statust']=$request->statust;
        $data['statusfo']=$request->statusfo;
        $data['orjonone']=$request->orjonone;
        $data['orjontwo']=$request->orjontwo;
        $data['orjonthree']=$request->orjonthree;
        $data['orjonfour']=$request->orjonfour;
        DB::table('assign_tasks')
            ->where('id', $request->id)
            ->update($data);
        Session::put('message', 'এসাইন টাস্ক সফলভাবে আপডেট হয়েছে!!');
        return redirect()->back();
    }

}
