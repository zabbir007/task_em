<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION))
{
    session_start();
}
date_default_timezone_set('Asia/Dhaka');
class SuperAdminController extends Controller
{
    public function __construct(){

        $this->middleware('checkAdmin');
    }
    //show dashboard
    public function superAdminDashboard(){
        $potibadonCount=DB::table('super_departments')
                            ->count();
        $karjokromCount=DB::table('departments')
                            ->count();
        $subkarjokromCount=DB::table('child_departments')
                            ->count();
        $employeeCount=DB::table('employees')
                            ->count();
        $taskCount=DB::table('tasks')
                        ->count();
        $assignTaskCount=DB::table('assign_tasks')
                            ->count();
        $employee_info=DB::table('employees')
                            ->get();
        $department_info=DB::table('departments')
                            ->get();
        // echo "<pre/>";
        // print_r($attendancce_data);
        // exit();
    	return view('admin.dashboard',compact('employee_info','department_info','potibadonCount','karjokromCount','subkarjokromCount','employeeCount','taskCount','assignTaskCount'));
    }
    //admin logout
    public function superAdminLogout(){
    	Session::put('adminId','');
        return redirect()->route('login_page');
    }
    //admin employee portion
    public function addEmployee(){
        return view('admin.employee.addEmployee');
    }
    public function showEmployee(){
        $employee_info=DB::table('employees')
                            ->get();
        return view('admin.employee.showEmployee',compact('employee_info'));
    }
    public function deleteEmployee(Request $request){
        $id=intval($request->id);
        DB::table('employees')
            ->where('id', $id)
            ->delete();
    }
    public function editEmployee($id){
        $singleInfo=DB::table('employees')
            ->where('id',$id)
            ->first();
        return view('admin.employee.editEmployee',compact('singleInfo'));
    }
    public function updateEmployeeData(Request $request){
        $id=$request->id;
        $employeeInfo=DB::table('employees')
                            ->where('id',$id)
                            ->first();
        $request->validate([
            'employee_email' => 'required',
            'employee_salary' => 'required',
            'employee_phone' => 'required',
            'employee_designation' => 'required'
        ]);
        $employee_email=$request->employee_email;
        $employee_salary=$request->employee_salary;
        $employee_phone=$request->employee_phone;
        $employee_designation=$request->employee_designation;
        $employee_password=$request->password;
        $data=array();
        $image=$request->file('employee_image');
        if ($image){
            $image_name=rand(100000,100000000);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='employee/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {
                $data['employee_image'] = $image_url;
            }
        }else{
            $data['employee_image'] = $employeeInfo->employee_image;
        }
        if ($employee_email){
            $data['employee_email'] = $employee_email;
        }else{
            $data['employee_email'] = $employeeInfo->employee_email;
        }
        if ($employee_salary){
            $data['employee_salary'] = $employee_salary;
        }else{
            $data['employee_salary'] = $employeeInfo->employee_salary;
        }
        if ($employee_phone){
            $data['employee_phone'] = $employee_phone;
        }else{
            $data['employee_phone'] = $employeeInfo->employee_phone;
        }
        if ($employee_designation){
            $data['employee_designation'] = $employee_designation;
        }else{
            $data['employee_designation'] = $employeeInfo->employee_designation;
        }
        if ($employee_password){
            $data['employee_password'] = md5($employee_password);
        }else{
            $data['employee_password'] = $employeeInfo->employee_password;
        }
        $data['updated_at']=date('Y-m-d');
        DB::table('employees')
            ->where('id',$id)
            ->update($data);
        Session::put('message','কর্মকর্তা সমূহ সফলভাবে আপডেট হয়েছে !!');
        return redirect()->back();
    }
    public function insertEmployeeData(Request $request){
        //laravel from validation
        $request->validate([
            'employee_email' => 'required',
            'employee_salary' => 'required',
            'employee_phone' => 'required',
            'employee_designation' => 'required',
            'password' => 'required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required',
        ]);
        //email check
        $checkEmail=DB::table('employees')
                        ->get();
        foreach($checkEmail as $checkE){
            if ($checkE->employee_email ==$request->employee_email) {
                Session::put('messageWarning','This Email Allready Used!!');
                return redirect()->back();
            }
        }
        //get data for array
        $data=array();
        $image=$request->file('employee_image');
        if ($image){
            $image_name=rand(100000,100000000);
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='employee/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {
                $data['employee_image'] = $image_url;
            }
        }
        $data['employee_email']=$request->employee_email;
        $data['employee_salary']=$request->employee_salary;
        $data['employee_phone']=$request->employee_phone;
        $data['employee_designation']=$request->employee_designation;
        $data['employee_password']=md5($request->password);
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $insertEmployeeData=DB::table('employees')
                                ->insert($data);
        if ($insertEmployeeData){
            Session::put('message','কর্মকর্তা সমূহ সফলভাবে ইনসার্ট হয়েছে!!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','কর্মকর্তা সমূহ সফলভাবে ইনসার্ট হয়নি!!!!!!');
            return redirect()->back();
        }
    }
}
