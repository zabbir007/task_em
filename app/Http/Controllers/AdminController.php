<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class AdminController extends Controller
{
	//use middleware to check admin login or not.
    public function __construct(){
        $this->middleware('checkAdminLogin');
    }
    //admin login page open
    public function login_page(){
        return view('admin.loginPage');
    }
    //check login
    public function check_login(Request $request){
    	//validation 
    	$request->validate([
		    'email' => 'required',
		    'password' => 'required',
		]);
		$email=$request->email;
    	$password=md5($request->password);
    	$singleAdmin = DB::table('admin')
		    				->where('email', $email)
		    				->where('password', $password)
		    				->first();
		if ($singleAdmin) {
			Session::put('adminId',$singleAdmin->id);
			return redirect()->route('superAdminDashboard');
		}else{
			Session::put('message','Email or Password Invalid');
			return redirect()->route('login_page');
		}
    }
}
